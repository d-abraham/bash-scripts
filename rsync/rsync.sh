include="$(pwd)/include-me"
exclude="$(pwd)/exclude-me"
src1=""
src2=""
dest=""

read -p "Would you like to try dry run src1 to dest? (y/n)  " runDry
if [[ $runDry = [Yy] ]]
then
	echo "Dry Run dest to src1..."
	rsync -avn --delete --include-from=$include --exclude-from=$exclude $src1 $dest
fi

read -p "Would you like to try dry run src2 to dest? (y/n)  " runDry
if [[ $runDry = [Yy] ]]
then
	echo "Dry Run dest to src2..."
	rsync -avn --delete --include-from=$include --exclude-from=$exclude $src2 $dest
fi

read -p "Would you like to excute? (y/n)  " excute
if [[ $excute = [Yy] ]]
then
	echo "Running src1 to dest..."
	rsync -av --delete --include-from=$include --exclude-from=$exclude $src1 $dest
	echo "Running src2 to dest..."
	rsync -av --delete --include-from=$include --exclude-from=$exclude $src2 $dest
	echo -e "$(date +%x_%X)" > "${dest}/Backup-Date"
	
fi
