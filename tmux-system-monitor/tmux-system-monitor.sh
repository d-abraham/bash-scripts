#!/bin/bash
#source arch wiki for tmux
tmux new-session -d -n Window1 htop

tmux split-window -h sudo nethogs
tmux selectp -t 1
tmux split-window -v ping google.com
tmux selectw -t 1
tmux split-window -v  watch "sensors | grep -e fan1 -e Core && hddtemp && free -hw"
tmux -2 attach-session -d
