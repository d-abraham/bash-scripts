#!/bin/bash
#source arch wiki for tmux
tmux new-session -d -n Window1 htop

tmux split-window -v sudo nethogs
tmux selectp -t 1
tmux split-window -v ping google.com
tmux selectw -t 1
tmux split-window -v  watch -n1 'lscpu | grep "CPU MHz" && sensors | grep -e fan1 -e fan4 -e fan2 -e SYSTIN -e CPUTIN -e SMBUSMASTER -e edge -e fan1 && hddtemp && free -hw'
tmux -2 attach-session -d

#For CPU:
# cat /proc/cpuinfo | grep "MHz" 
# lscpu | grep 'CPU MHz'
