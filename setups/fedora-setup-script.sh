#!/bin/bash
#add rpmfusion and flatpak repo before running the script
dnf install chromium libgnome-keyring thunderbird-wayland lm_sensors hddtemp syncthing vim grsync krita scribus vlc gnome-tweaks transmission keepassxc fzf steam -y
dnf group install --with-optional virtualization -y
systemctl enable libvirtd
flatpak install flathub com.spotify.Client flathub org.signal.Signal -y

#Syncthing run those command after 
#$ syncthing
#$ systemctl --user enable --now syncthing.service
#$ sensors-detect

#vim and tmux
#copy vimrc and .tmux.conf and setup plugins
#youcompleteme setup, install
#sudo dnf install golang cmake gcc-c++ make python3-devel nodejs
#cd ~/.vim/../../youcompleteme
#python3 install.py --all
