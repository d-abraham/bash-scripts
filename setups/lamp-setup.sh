#!/bin/bash
echo "Installing LAMP"
echo "Installing MariaDB"
dnf install mariadb mariadb-server
if [ $? == 0 ]; then
	echo "Complete"
else 
	echo "Failed"
fi 
echo "Installing php and httpd"
dnf install httpd php php-common
if [ $? == 0 ]; then
	echo "Complete"
else 
	echo "Failed"
fi 
echo "installing php extra stuff..."
dnf install phpmyadmin php-pecl-apcu php-cli php-pear php-pdo php-mysqlnd php-pgsql php-pecl-mongodb php-pecl-memcache php-pecl-memcached php-gd php-mbstring php-mcrypt php-xml
if [ $? == 0 ]; then
	echo "Complete"
else 
	echo "Failed"
fi 
echo "starting Mariadb & running instalation"
systemctl start mariadb.service
/usr/bin/mysql_secure_installation
echo "starting httpd server" 
systemctl start httpd.service
echo "changing www group and permission, so that you can read and write"
chgrp -R wheel /var/www
chmod -R 775 /var/www
